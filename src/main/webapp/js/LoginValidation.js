function submitValidation() {
    var acc = document.getElementById("acc").value;
    var pass = document.getElementById("pass").value;

    if (acc.length == 5 && acc.length > 0) {
        document.getElementById("accErr").innerHTML = "*Success";
        if (pass.length == 5 && pass.length > 0) {
            document.getElementById("passErr").innerHTML = "*Success";
            return true;
        } else {
            document.getElementById("passErr").innerHTML = "*Fail";
            return false;
        }
    } else {
        document.getElementById("accErr").innerHTML = "*Fail";
        return false;
    }
}