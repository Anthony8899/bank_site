<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%
Object user = session.getAttribute("user");
%>
<title>Welcome ${user.name}</title>
</head>
<body>
	<jsp:include page="MenuBar.jsp"></jsp:include>
	<jsp:include page="ShowUser.jsp"></jsp:include>
</body>
</html>