<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/AddBeneficiary.css">
</head>
<title>Add Beneficiary</title>
</head>
<body>
	<jsp:include page="MenuBar.jsp"></jsp:include>
	<jsp:include page="ShowUser.jsp"></jsp:include>

	<div class="container">
		<div class="col-lg-12">
			<form action="${pageContext.request.contextPath}/ben" class="form">
				<div class="col-sm-offset-3 col-lg-6 col-sm-offset-3 check">
					<h2>Add Beneficiary</h2>
					<div class="nameDiv">
						<input type="text" name="holderName" id="holderName"
							class="form-control" placeholder="Enter Account Holder Name.">
					</div>
					<div class="accNoDiv">
						<input type="text" name="accNo" id="accNo" class="form-control"
							placeholder="Enter Account No.">
					</div>
					<div class="ifscCode">
						<input type="text" name="ifsc" id="ifsc" class="form-control"
							placeholder="Enter IFSC code.">
					</div>
					<div class="btns">
						<button type="reset" class="btn btn-warning">Reset</button>
						<button type="submit" class="btn btn-success">Add</button>
					</div>
				</div>
			</form>
			
		</div>
		
				<div>
					<div class="panel-body"></div>
				</div>

				<c:if test="${msg!=null}">
					<div class="panel panel-default">
						<div class="panel-body">${msg}</div>
					</div>
				</c:if>
			
	</div>
</body>
</html>