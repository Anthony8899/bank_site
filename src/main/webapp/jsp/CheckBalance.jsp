<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Balance Checking</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/CheckBalance.css">
</head>

<body>
	<jsp:include page="MenuBar.jsp"></jsp:include>
	<jsp:include page="ShowUser.jsp"></jsp:include>
	<div class="container">
		<div class="col-lg-12">
			<form action="${pageContext.request.contextPath}/CheckBalance"
				class="form">
				<div class="col-sm-offset-3 col-lg-6 col-sm-offset-3 check">
					<h2>Check Balance</h2>
					<div class="passDiv">
						<input type="password" name="pass" id="pass" class="form-control"
							placeholder="Enter Your Password.">
					</div>
					<div>
						<button type="submit" class="btn btn-success">Check
							Balance</button>
					</div>

					<c:if test="${cb.status=='1'}">
						<div class="panel panel-primary">
							<div class="panel-heading">Your Account Balance is....</div>
							<div class="panel-body">Rs. ${cb.amount}/-</div>
						</div>
					</c:if>
					<c:if test="${status=='0'}">
						<div class="panel panel-primary">
							<div class="panel-heading">UserName & Password Incorrect.</div>
						</div>
					</c:if>
				</div>
			</form>
		</div>
	</div>
</body>

</html>