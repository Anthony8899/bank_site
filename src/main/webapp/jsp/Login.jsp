<!-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%> -->
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/Login.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/LoginValidation.js"></script>
	<%-- <script type="text/javascript" src="../js/LoginValidation.js"></script> --%>
<title>Login</title>
</head>

<body>
	<jsp:include page="MenuBar.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="col-lg-12">
			<div class="col-sm-offset-4 col-lg-4 col-lg-offset-4 pos">
				<h2>Customer Login</h2>
				<form action="${pageContext.request.contextPath}/LoginServlet" method="post"><!-- onsubmit="return submitValidation()" -->
					<div class="form-group">
						<input type="text" class="form-control" name="acc" id="acc"
							placeholder="Enter Account Number" onblur="accValidation()">
						<p id="accErr" style="color: red; margin-left: 10px;"></p>
					</div>

					<div class="form-group">
						<input type="text" class="form-control" name="pass" id="pass"
							placeholder="Enter Password" onblur="passValidation()">
							<p id="passErr" style="color: red; margin-left: 10px;"></p>
					</div>

					<div>
						<p style="text-align: right;">
							<a href="#" class="new">Forget Password?</a>
						</p>
					</div>

					<div class="btns">
						<button class="btn btn-info btn-md res">Reset</button>
						<button class="btn btn-success btn-md suc">Login</button>
					</div>

					<div>
						<h5>
							New User? <a
								href="${pageContext.request.contextPath}/jsp/Registration.jsp"
								class="new">Click Here</a>
						</h5>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

</html>