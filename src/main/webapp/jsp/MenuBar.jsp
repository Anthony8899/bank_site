<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">XYZ Bank</a>
			</div>
			<%
			Object user = session.getAttribute("user");
			%>
			<c:if test="${user.accNo != null}">
				<ul class="nav navbar-nav">

					<li><a
						href="${pageContext.request.contextPath}/jsp/CheckBalance.jsp">Check
							Balance </a></li>

					<li><a
						href="${pageContext.request.contextPath}/GetBene">Money
							Transfer</a></li>
					<li><a
						href="${pageContext.request.contextPath}/jsp/AddBeneficiary.jsp">Add
							Beneficiary</a></li>
				</ul>
			</c:if>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Contact US</a></li>
				<c:if test="${user.accNo == null}">
					<li><a href="${pageContext.request.contextPath}/jsp/Login.jsp">Login</a></li>
				</c:if>
				<c:if test="${user.accNo != null}">
					<li><a href="${pageContext.request.contextPath}/LogoutServlet">Logout</a></li>
				</c:if>
			</ul>
		</div>
	</nav>
</body>
</html>