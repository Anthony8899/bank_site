<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/Registration.css">
    <title>Customer Registration</title>
</head>

<body>
<jsp:include page="MenuBar.jsp"></jsp:include>
    <div class="container">
        <div class="col-lg-12">
            <form action="${pageContext.request.contextPath}/RegistrationServlet" method="post">
                <div id="form">
                    <h2>Customer Registration</h2>
                    <div class="col-sm-3">
                        <input type="text" name="cusId" id="cusId" placeholder="Customer ID" class="form-control">
                    </div>

                    <div class="col-sm-3">
                        <input type="text" name="accNo" id="accNo" placeholder="Account No." class="form-control">
                    </div>
                    
                    <div class="col-sm-3">
                        <input type="text" name="ifsc" id="ifsc" placeholder="IFSC Code." class="form-control">
                    </div>

                    <div class="col-sm-3">
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control">
                        <p>${err.nameErr}</p>
                    </div>

                    <div class="col-sm-3">
                        <input type="text" name="age" id="age" placeholder="Age" class="form-control">
                        <p>${err.ageErr}</p>
                    </div>

                    <div class="col-sm-3">
                        <input type="text" name="con" id="con" placeholder="Contact Number" class="form-control">
                    	<p>${err.conErr}</p>
                    </div>



                    <div class="col-sm-2">
                        <input type="text" name="email" id="email" placeholder="Email" class="form-control">
                    </div>

                    <div class="col-sm-2">
                        <input type="text" name="pass" id="pass" placeholder="Enter Your Password" class="form-control">
                    </div>

                    <div class="col-sm-2">
                        <input type="text" name="conPass" id="conPass" placeholder="Re Enter Password"
                            class="form-control">
                    </div>

                    <div class="col-sm-6">
                        <select name="secQues" id="secQues" class="form-control">
                            <Option value="What is your Favourite Cricketer?">What is your Favourite Cricketer?</Option>
                            <Option value="What is your favourite Foot Ball Team?">What is your favourite Foot Ball
                                Team?
                            </Option>
                            <Option value="What is your Native?">What is your Native?</Option>
                        </select>
                    </div>

                    <div class="col-sm-6">
                        <input type="text" name="ans" id="ans" placeholder="Security Answer" class="form-control">
                    </div>

                    <div class="col-sm-12">
                        <input type="text" name="aadhar" id="aadhar" placeholder="Aadhar Number" class="form-control">
                    </div>
                    <div class="col-sm-12">
                        <textarea name="add" id="add" cols="30" rows="3" class="form-control"></textarea>
                    </div>
                    <div id="btns">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
</body>

</html>