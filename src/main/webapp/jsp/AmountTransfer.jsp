<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/AmountTransfer.css">
<title>Amount Transfer</title>
</head>

<body>
	<jsp:include page="MenuBar.jsp"></jsp:include>
	<jsp:include page="ShowUser.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="col-lg-12 sub-container">

			<div class="form" class="col-lg-7">
				<div class="col-lg-12 mx-auto">
					<form action="${pageContext.request.contextPath}/TransferServlet"
						method="post">
						<div class="col-lg-12 check">
							<div>
								<h2>Amount Transfer</h2>
							</div>

							<div class="col-lg-10">
								<input type="text" name="accNo" id="accNo"
									placeholder="Account Number" class="form-control"
									value="${accNo}">
							</div>

							<div class="col-lg-10">
								<input type="text" name="ifsc" id="ifsc" placeholder="IFSC Code"
									class="form-control" value="${ifsc}">
							</div>

							<div class="col-lg-10">
								<input type="text" name="amount" id="amount"
									placeholder="Amount" class="form-control">
							</div>

							<div class="col-lg-10">
								<input type="password" name="pass" id="pass"
									placeholder="Enter Your Password" class="form-control">
							</div>
							<div class="btns col-sm-10">
								<button type="reset" class="btn btn-warning">Reset</button>
								<button type="submit" class="btn btn-success">Transfer</button>
							</div>


						</div>

					</form>


				</div>
				<div>
					<div class="panel-body"></div>
				</div>

				<c:if test="${message!=null}">
					<div class="panel panel-default">
						<div class="panel-body">${message}</div>
					</div>
				</c:if>

			</div>



			<div class="ben col-sm-4">
				<h4>Beneficiaries</h4>
				<hr>
				<c:if test="${isThere==true}">
					<c:forEach items="${benes}" var="ben">
						<div class="panel panel-success">
							<div class="panel-heading">${ben.holderName}</div>
							<div class="panel-body">
								<p>${ben.accNo}</p>
								<p>${ben.ifsc}</p>
								<a href="#">Delete</a> <a
									href="${pageContext.request.contextPath}/setbene?accNo=${ben.accNo}&ifsc=${ben.ifsc}">Use</a>
							</div>
						</div>
					</c:forEach>
				</c:if>
				<c:if test="${isThere==false}">
					<div class="panel panel-success">
						<div class="panel-heading">No Data Found.</div>
					</div>
				</c:if>
			</div>

		</div>
	</div>
</body>

</html>