package Validation;

import java.util.regex.Pattern;

public class ValidationClass {

	public static boolean charValidation(String val) {
		if (Pattern.matches("^[a-zA-Z]+$", val)) {
			return true;
		}
		return false;
	}

	public static boolean ageValidation(int age) {
		if (age > 0 && age < 100) {
			return true;
		}
		return false;
	}
	
	public static boolean conValidation(String con) {
		if (Pattern.matches("^\\d{10}$", con)) {
			return true;
		}
		return false;
	}
}
