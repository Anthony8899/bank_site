package filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import Validation.ValidationClass;

/**
 * Servlet Filter implementation class RegistrationValidation
 */
@WebFilter(urlPatterns = "/RegistrationValidation", servletNames = { "registrationSer" })
public class RegistrationValidation implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String name = request.getParameter("name");
		String age = request.getParameter("age");
		String con = request.getParameter("con");
		Map<String, String> errors = new HashMap<>();
		if (!ValidationClass.charValidation(name)) {
			errors.put("nameErr", "Only Characters Should be Required.");
		}
		if (!ValidationClass.ageValidation(Integer.parseInt(age))) {
			errors.put("ageErr", "Check Your Age.");
		}
		if (!ValidationClass.conValidation(con)) {
			errors.put("conErr", "Check Your Mobile Number.");
		}
		if (errors.isEmpty()) {
			chain.doFilter(request, response);
		} else {
			((HttpServletRequest) request).setAttribute("err", errors);
			request.getRequestDispatcher("jsp/Registration.jsp").forward(request, response);
		}
	}

}
