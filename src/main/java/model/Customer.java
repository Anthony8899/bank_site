package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Customer {

	private String custID;
	private String accNo;
	private String name;
	private int age;
	private String contact;
	private String email;
	private String password;
	private String secQues;
	private String secAns;
	private String aadhar;
	private String address;
	private String ifscCode;
}
