package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Beneficiary {

	private String holderName;
	private String accNo;
	private String ifsc;
}
