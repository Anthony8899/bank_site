package dao;

import static config.Configuration.DRIVER_CLASS;
import static config.Configuration.PASS;
import static config.Configuration.URL;
import static config.Configuration.USER;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Beneficiary;
import model.Customer;

public class BankCrudDAO {
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public boolean register(Customer cust) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement(
					"insert into customers (cust_id, acc_no, name, age, contact, email, password, sec_ques, sec_ans, aadhar, address, ifsc_no) values(?,?,?,?,?,?,?,?,?,?,?,?)");

			ps.setString(1, cust.getCustID());
			ps.setString(2, cust.getAccNo());
			ps.setString(3, cust.getName());
			ps.setInt(4, cust.getAge());
			ps.setString(5, cust.getContact());
			ps.setString(6, cust.getEmail());
			ps.setString(7, cust.getPassword());
			ps.setString(8, cust.getSecQues());
			ps.setString(9, cust.getSecAns());
			ps.setString(10, cust.getAadhar());
			ps.setString(11, cust.getAddress());
			ps.setString(12, cust.getIfscCode());

			int i = ps.executeUpdate();
			if (i > 0) {
				ps = con.prepareStatement("insert into customers_amount values(?,?)");
				ps.setString(1, cust.getCustID());
				ps.setLong(2, 0);
				int j = ps.executeUpdate();
				if (j > 0) {
					return true;
				} else {
					return false;
				}

			}

		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return false;
	}

	public Map<String, Object> login(Map<String, String> log) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("select cust_id, acc_no, name from customers where acc_no=? and password=?");
			ps.setString(1, log.get("accNo"));
			ps.setString(2, log.get("pass"));
			rs = ps.executeQuery();
			if (rs.next()) {

				if (log.get("proc").equals("LN")) {
					Map<String, Object> user = new HashMap<>();
					user.put("accNo", rs.getString("acc_no"));
					user.put("cust_id", rs.getString("cust_id"));
					user.put("name", rs.getString("name"));
					return user;
				}

				if (log.get("proc").equals("CB")) {
					Long holdingAmount = checkBalance(log.get("cust_id"));
					Map<String, Object> cb = new HashMap<>();
					cb.put("amount", holdingAmount);
					cb.put("status", "1");
					return cb;
				}
				if (log.get("proc").equals("AT")) {
					Long holdingAmount = checkBalance(log.get("cust_id"));
					Long amount = Long.parseLong(log.get("amount"));
					Map<String, Object> resp = new HashMap<>();
					if (holdingAmount > amount) {
						resp.put("status", "true");
					} else {
						resp.put("status", "false");
					}
					return resp;
				}

			}

		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return null;
	}

	private Long checkBalance(String custId) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("select amount from customers_amount where cust_id=?");
			ps.setString(1, custId);
			rs = ps.executeQuery();
			if (rs.next()) {
				long amount = rs.getLong("amount");
				return amount;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return null;

	}

	public boolean amountTransfer(Map<String, String> log) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("select cust_id from customers where acc_no=? and ifsc_no=?");
			ps.setString(1, log.get("accNo"));
			ps.setString(2, log.get("ifsc"));
			rs = ps.executeQuery();
			if (rs.next()) {
				String custId = rs.getString("cust_id");
				Long holdingAmount = checkBalance(custId);
				Long insertingAmount = holdingAmount + Long.parseLong(log.get("amount"));
				ps = con.prepareStatement("update customers_amount set amount=? where cust_id=?");
				ps.setLong(1, insertingAmount);
				ps.setString(2, custId);
				int i = ps.executeUpdate();
				if (i > 0) {
					String holderCustId = log.get("cust_id");
					holdingAmount = checkBalance(holderCustId);
					Long amount = Long.parseLong(log.get("amount"));
					Long updatingAmount = holdingAmount - amount;
					ps = con.prepareStatement("update customers_amount set amount=? where cust_id=?");
					ps.setLong(1, updatingAmount);
					ps.setString(2, holderCustId);
					int j = ps.executeUpdate();
					if (j > 0) {
						return true;
					}
				}
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return false;
	}

	public boolean addBen(Map<String, String> ben) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("insert into beneficiaries values(?,?,?,?)");

			ps.setString(1, ben.get("cust_id"));
			ps.setString(2, ben.get("holName"));
			ps.setString(3, ben.get("accNo"));
			ps.setString(4, ben.get("ifsc"));
			int i = ps.executeUpdate();
			if (i > 0) {
				return true;
			} else {
				return false;
			}

		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return false;
	}

	public List<Beneficiary> getBene(String custId) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("select * from beneficiaries where cust_id=?");
			ps.setString(1, custId);
			rs = ps.executeQuery();
			List<Beneficiary> li = new ArrayList<>();
			Beneficiary ben;
			while (rs.next()) {
				ben = new Beneficiary();
				ben.setHolderName(rs.getString("holder_name"));
				ben.setAccNo(rs.getString("acc_no"));
				ben.setIfsc(rs.getString("ifsc"));
				li.add(ben);
			}
			if (!li.isEmpty()) {
				return li;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return null;
	}

}
