package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BankCrudDAO;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Map<String, String> log = new HashMap<>();
		log.put("accNo", req.getParameter("acc"));
		log.put("pass", req.getParameter("pass"));
		log.put("proc", "LN");
		BankCrudDAO dao = new BankCrudDAO();
		Map<String, Object> user = dao.login(log);
		if (user != null) {
			HttpSession sess = req.getSession();
			sess.setAttribute("user", user);
			res.sendRedirect("jsp/Home.jsp");
		} else {
			req.getRequestDispatcher("jsp/Login.jsp").include(req, res);
		}

	}

}
