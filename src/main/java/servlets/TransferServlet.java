package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BankCrudDAO;

/**
 * Servlet implementation class TransferServlet
 */
@WebServlet("/TransferServlet")
public class TransferServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String pass = req.getParameter("pass");
		String amount = req.getParameter("amount");
		String accNum = req.getParameter("accNo");
		String ifscCode = req.getParameter("ifsc");
		HttpSession sess = req.getSession();
		Map<String, String> user = (Map<String, String>) sess.getAttribute("user");
		String acc = user.get("accNo");
		String cust_id = user.get("cust_id");
		Map<String, String> log = new HashMap<>();
		log.put("accNo", acc);
		log.put("pass", pass);
		log.put("amount", amount);
		log.put("cust_id", cust_id);
		log.put("proc", "AT");
		BankCrudDAO dao = new BankCrudDAO();
		Map<String, Object> resp = dao.login(log);
		String status = String.valueOf(resp.get("status"));
		if (status.equals("true")) {
			log.clear();
			log.put("accNo", accNum);
			log.put("ifsc", ifscCode);
			log.put("cust_id", cust_id);
			log.put("amount", amount);
			boolean s = dao.amountTransfer(log);
			if (s) {
				System.out.println("Transfered successfull");
				req.setAttribute("message", "Money Transfered Successfully.");
				req.getRequestDispatcher("GetBene").forward(req, res);
			}else {
				System.out.println("Transfer Unsuccessfull");
			}
		} else {
			System.out.println("Everything is not fine!!!!");
			req.getRequestDispatcher("GetBene").forward(req, res);
		}

	}

}
