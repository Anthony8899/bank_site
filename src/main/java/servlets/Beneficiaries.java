package servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BankCrudDAO;

@WebServlet("/ben")
public class Beneficiaries extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String holName = req.getParameter("holderName");
		String accNo = req.getParameter("accNo");
		String ifsc = req.getParameter("ifsc");
		HttpSession sess = req.getSession();
		Map<String, String> user = (Map<String, String>) sess.getAttribute("user");
		String cust_id = user.get("cust_id");

		user.clear();
		user.put("cust_id", cust_id);
		user.put("holName", holName);
		user.put("ifsc", ifsc);
		user.put("accNo", accNo);
		BankCrudDAO dao = new BankCrudDAO();
		boolean status = dao.addBen(user);
		if (status) {
			req.setAttribute("msg", "Successfully Added.");
			req.getRequestDispatcher("jsp/AddBeneficiary.jsp").forward(req, res);
		} else {
			req.setAttribute("msg", "Failed.");
			req.getRequestDispatcher("jsp/AddBeneficiary.jsp").forward(req, res);
		}

	}

}
