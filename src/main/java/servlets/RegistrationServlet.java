package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BankCrudDAO;
import model.Customer;

/**
 * Servlet implementation class RegistrationServlet
 */

@WebServlet(urlPatterns =  "/RegistrationServlet", name = "registrationSer")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Customer cust = new Customer();
		cust.setCustID(req.getParameter("cusId"));
		cust.setAccNo(req.getParameter("accNo"));
		cust.setIfscCode(req.getParameter("ifsc"));
		cust.setName(req.getParameter("name"));
		cust.setAge(Integer.parseInt(req.getParameter("age")));
		cust.setContact(req.getParameter("con"));
		cust.setEmail(req.getParameter("email"));
		cust.setPassword(req.getParameter("pass"));
		String conPass = req.getParameter("conPass");
		cust.setSecQues(req.getParameter("secQues"));
		cust.setSecAns(req.getParameter("ans"));
		cust.setAadhar(req.getParameter("aadhar"));
		System.out.println(req.getParameter("add"));
		cust.setAddress(req.getParameter("add"));

		if (cust.getPassword().equals(conPass)) {
			BankCrudDAO dao = new BankCrudDAO();
			boolean status = dao.register(cust);
			if (status) {
				res.sendRedirect("jsp/Login.jsp");
			} else {
				System.out.println("Insertion Failed :(");
			}
		} else {
			String url = "jsp/Registration.jsp";
			req.getRequestDispatcher(url).forward(req, res);
		}
	}

}
