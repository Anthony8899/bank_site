package servlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BankCrudDAO;
import model.Beneficiary;

@WebServlet("/GetBene")
public class GetBene extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sess = request.getSession();
		Map<String, String> user = (Map<String, String>) sess.getAttribute("user");
		String cust_id = user.get("cust_id");
		BankCrudDAO dao = new BankCrudDAO();
		List<Beneficiary> bene = dao.getBene(cust_id);
		if (bene != null) {
			request.setAttribute("benes", bene);
			request.setAttribute("isThere", true);
			request.getRequestDispatcher("jsp/AmountTransfer.jsp").forward(request, response);
		} else {
			request.setAttribute("isThere", false);
			request.getRequestDispatcher("jsp/AmountTransfer.jsp").forward(request, response);
		}
	}

}
