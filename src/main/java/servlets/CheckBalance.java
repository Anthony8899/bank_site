package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BankCrudDAO;

@WebServlet("/CheckBalance")
public class CheckBalance extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String pass = req.getParameter("pass");
		HttpSession sess = req.getSession();
		Map<String, String> user = (Map<String, String>) sess.getAttribute("user");
		String acc = user.get("accNo");
		String cust_id = user.get("cust_id");
		BankCrudDAO dao = new BankCrudDAO();
		Map<String, String> log = new HashMap<>();
		log.put("accNo", acc);
		log.put("cust_id", cust_id);
		log.put("pass", pass);
		log.put("proc", "CB");
		Map<String, Object> checkBalance = dao.login(log);
		if (checkBalance != null) {
			req.setAttribute("cb", checkBalance);
			String url = "jsp/CheckBalance.jsp";
			req.getRequestDispatcher(url).forward(req, res);
		}
		else {
			req.setAttribute("status", "0");
			String url = "jsp/CheckBalance.jsp";
			req.getRequestDispatcher(url).forward(req, res);
		}

		System.out.println("acc: " + acc + " : pass: " + pass);
	}

}
