package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/setbene")
public class SetBeneToTransfer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String accNo = request.getParameter("accNo");
		String ifsc = request.getParameter("ifsc");
		request.setAttribute("accNo", accNo);
		request.setAttribute("ifsc", ifsc);
		request.getRequestDispatcher("GetBene").forward(request, response);
//		request.getRequestDispatcher("jsp/AmountTransfer.jsp").forward(request, response);

	}

}
